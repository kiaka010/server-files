#! /bin/bash

docker volume create portainer_data
docker run -d -p 8000:8000 -p 9000:9000 --name portainer --env VIRTUAL_HOST=portainer.tiamat --env VIRTUAL_PORT=9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
